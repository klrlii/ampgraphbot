import math
import os

import numpy as np
import sympy as sp
import matplotlib.pyplot as plt
from scipy.interpolate import Rbf


class Processor:
    def __init__(self) -> None:
        pass

    def calc(self, expr: str) -> str:
        return str(sp.sympify(expr).evalf())

    def func_val_in_point(self, expr) -> str:
        splitted = expr.split(";")
        function = sp.sympify(splitted[0])
        values = splitted[1].split()
        x = sp.Symbol("x")
        out = []
        for val in values:
            out.append(function.subs(x, float(val)))
        result = ""
        for elem in out:
            result += f"{str(elem)}\n"
        return result.rstrip()

    def to_scientific(self, values: str) -> str:
        values = values.split()
        optimised = ""
        for value in values:
            if float(value) == 0.0:
                optimised += "0.00\n"
            elif abs(float(value)) > 100 or abs(float(value)) < 1:
                exp = int(math.log10(abs(float(value))))
                coef = float(value) / float(10**exp)
                optimised += "{:.2f}*10^{}\n".format(coef, exp)
            else:
                optimised += "{:.2f}\n".format(float(value))
            if "*10^0" in optimised:
                optimised = optimised.replace("*10^0", "")
        return optimised.rstrip()

    def undefind_integral(self, integrals: str) -> str:
        out = ""
        integrals = integrals.split("\n")
        x = sp.Symbol("x")
        for integral in integrals:
            out += f"{sp.integrate(integral, x)} + C\n"
        return out.rstrip()

    def integral(self, integrals: str) -> str:
        x = sp.Symbol("x")
        integrals = integrals.split("\n")
        integrals_ = []
        bounds = []
        for integral in integrals:
            integrals_.append(integral.split(";")[0])
            bounds.append(integral.split(";")[1])
        lower_bounds = []
        upper_bounds = []
        for bound in bounds:
            lower_bounds.append(bound.split()[0])
            upper_bounds.append(bound.split()[1])
        out = ""
        for i, lb, ub in zip(integrals_, lower_bounds, upper_bounds):
            out += f"{str(sp.integrate(i, (x, float(lb), float(ub))))}\n"
        return out.rstrip()

    def std(self, values: str) -> str:
        values = values.split()
        nums = []
        for val in values:
            nums.append(float(val))
        nums = np.array(nums)
        return str(nums.std())

    def mean(self, values: str) -> str:
        values = values.split()
        nums = []
        for val in values:
            nums.append(float(val))
        nums = np.array(nums)
        return str(nums.mean())

    def derr(self, exprs: str) -> str:
        exprs = exprs.split("\n")
        x = sp.Symbol("x")
        out = ""
        for expr in exprs:
            out += f"{sp.diff(expr, x)}\n"
        return out.rstrip()

    def deg_to_rad(self, x):
        return x * np.pi / 180

    def rad_to_deg(self, x):
        return x * 180 / np.pi

    def interp(self, data: str, name_param: str) -> str:
        data = data.split("\n")
        X_str = []
        Y_str = []
        for elem in data:
            elem = elem.split(";")
            if len(elem) == 2:
                X_str.append(elem[0])
                Y_str.append(elem[1])
        X = []
        Y = []
        for x, y in zip(X_str, Y_str):
            x_ = []
            y_ = []
            for i, j in zip(x.split(), y.split()):
                x_.append(float(i))
                y_.append(float(j))
            X.append(x_)
            Y.append(y_)

        fig, ax = plt.subplots()
        ax.grid(True)

        for x, y, i in zip(X, Y, range(len(X))):
            rbf = Rbf(x, y)
            x_plt = np.linspace(x[0], x[-1], 500)
            y_plt = rbf(x_plt)
            ax.plot(x_plt, y_plt, label=f"{i + 1}")
            ax.scatter(x, y)

        ax2 = ax.secondary_xaxis("top", functions=(self.rad_to_deg, self.deg_to_rad))
        ax.legend()
        ax.set_xlabel("$\Deltaln(T)$")
        ax2.set_xlabel("X (degrees)")
        ax.set_ylabel("$ln(\ksi)$")
        ax2.set_xlim(ax.get_xlim())
        if f"{name_param}" in os.listdir("./users_graphics"):
            plt.savefig(f"./users_graphics/{name_param}/INTERP.png")
        else:
            os.mkdir(f"./users_graphics/{name_param}")
            plt.savefig(f"./users_graphics/{name_param}/INTERP.png")
        plt.close("all")

        return f"./users_graphics/{name_param}/INTERP.png"

    def approx(self, data: str, name_param: str) -> str:
        data = data.split("\n")
        X_str = []
        Y_str = []
        degrees_str = []
        for elem in data:
            elem = elem.split(";")
            if len(elem) == 2:
                X_str.append(elem[0])
                Y_str.append(elem[1])
                degrees_str.append(None)
            elif len(elem) == 3:
                X_str.append(elem[0])
                Y_str.append(elem[1])
                degrees_str.append(elem[2])
        X = []
        Y = []
        degree = []
        for x, y, d in zip(X_str, Y_str, degrees_str):
            x_ = []
            y_ = []
            for i, j in zip(x.split(), y.split()):
                x_.append(float(i))
                y_.append(float(j))
            X.append(x_)
            Y.append(y_)
            if d is not None:
                degree.append(float(d))
            else:
                degree.append(3)

        fig, ax = plt.subplots()
        ax.grid(True)

        for x, y, d, i in zip(X, Y, degree, range(len(degree))):
            coef = np.polyfit(x, y, d)
            f = np.poly1d(coef)
            x_plt = np.linspace(x[0], x[-1], 500)
            y_plt = f(x_plt)
            ax.plot(x_plt, y_plt, label=f"degree for {i + 1} : {d}")
            ax.scatter(x, y)

        ax2 = ax.secondary_xaxis("top", functions=(self.rad_to_deg, self.deg_to_rad))
        ax.legend()
        ax.set_xlabel("X (radians)")
        ax2.set_xlabel("X (degrees)")
        ax.set_ylabel("f(X)")
        ax2.set_xlim(ax.get_xlim())
        if f"{name_param}" in os.listdir("./users_graphics"):
            plt.savefig(f"./users_graphics/{name_param}/APPROX.png")
        else:
            os.mkdir(f"./users_graphics/{name_param}")
            plt.savefig(f"./users_graphics/{name_param}/APPROX.png")
        plt.close("all")

        return f"./users_graphics/{name_param}/APPROX.png"

    def poly_expr(self, data: str):
        data = data.split("\n")
        X_str = []
        Y_str = []
        degrees_str = []
        for elem in data:
            elem = elem.split(";")
            if len(elem) == 2:
                X_str.append(elem[0])
                Y_str.append(elem[1])
                degrees_str.append(None)
            elif len(elem) == 3:
                X_str.append(elem[0])
                Y_str.append(elem[1])
                degrees_str.append(elem[2])
        X = []
        Y = []
        degree = []
        for x, y, d in zip(X_str, Y_str, degrees_str):
            x_ = []
            y_ = []
            for i, j in zip(x.split(), y.split()):
                x_.append(float(i))
                y_.append(float(j))
            X.append(x_)
            Y.append(y_)
            if d is not None:
                degree.append(float(d))
            else:
                degree.append(3)
        out = ""
        for x, y, d, i in zip(X, Y, degree, range(len(degree))):
            coef = np.polyfit(x, y, d)
            line = ""
            for j, c in zip(range(len(coef)), coef):
                if j == 0:
                    line += f"{round(c, 4)} * x^{len(coef) - j - 1}"
                elif len(coef) == j + 1:
                    line += f" + {round(c, 4)}"
                else:
                    line += f" + {round(c, 4)} * x^{len(coef) - j - 1}"
            out += f"{line}\n"
        out = str(out)
        out = out.replace("+ -", "- ")
        out = out.replace("^1", "")
        return out.rstrip()

    def simple(self, data: str, name_param) -> str:
        exprs = data.split("\n")
        functions = []
        bounds = []
        x_s = sp.Symbol("x")
        for expr in exprs:
            expr = expr.split(";")
            functions.append(expr[0])
            if len(expr) == 2:
                bounds.append(expr[1])
            else:
                bounds.append(None)

        sp_f = []
        for func in functions:
            sp_f.append(sp.sympify(func))

        lower_bound = []
        upper_bound = []
        for bound in bounds:
            if bound is not None:
                bound = bound.split()
                lower_bound.append(bound[0])
                if len(bound) == 2:
                    upper_bound.append(bound[1])
                elif len(bound) == 1:
                    upper_bound.append(None)
            else:
                lower_bound.append(None)
                upper_bound.append(None)

        lower_bound_floated = [None] * len(sp_f)
        upper_bound_floated = [None] * len(sp_f)
        for lb, ub, i in zip(lower_bound, upper_bound, range(len(sp_f))):
            if lb is not None:
                lower_bound_floated[i] = float(lb)
            if ub is not None:
                upper_bound_floated[i] = float(ub)

        has_lb = False
        for lb in lower_bound_floated:
            if lb != None:
                has_lb = True

        has_ub = False
        for ub in upper_bound_floated:
            if ub != None:
                has_ub = True

        lbUnNoned = []
        ubUnNoned = []
        for lb, ub in zip(lower_bound_floated, upper_bound_floated):
            if lb is not None:
                lbUnNoned.append(lb)
            if ub is not None:
                ubUnNoned.append(ub)
        if has_lb == False:
            for i in range(len(sp_f)):
                lower_bound_floated[i] = -10
                upper_bound_floated[i] = 10
        elif has_ub == False:
            if max(lbUnNoned) != min(lbUnNoned):
                for i in range(len(sp_f)):
                    upper_bound_floated[i] = (
                        max(lbUnNoned) + (max(lbUnNoned) - min(lbUnNoned)) / 2
                    )
            else:
                for i in range(len(sp_f)):
                    upper_bound_floated[i] = 10
            for lb, i in zip(lower_bound_floated, range(len(sp_f))):
                if lb is None:
                    lower_bound_floated[i] = min(lbUnNoned)
        elif has_ub == True:
            for lb, ub, i in zip(
                lower_bound_floated, upper_bound_floated, range(len(sp_f))
            ):
                if lb is None:
                    if max(lbUnNoned) > max(ubUnNoned):
                        lower_bound_floated[i] = min(lbUnNoned)
                        upper_bound_floated[i] = (
                            max(lbUnNoned) - min(lbUnNoned)
                        ) / 2 + max(lbUnNoned)
                    else:
                        lower_bound_floated[i] = min(lbUnNoned)
                        upper_bound_floated[i] = max(ubUnNoned)
                elif ub is None:
                    if max(lbUnNoned) > max(ubUnNoned):
                        upper_bound_floated[i] = (
                            max(lbUnNoned) - min(lbUnNoned)
                        ) / 2 + max(lbUnNoned)
                    else:
                        upper_bound_floated[i] = max(ubUnNoned)

        X = []
        for lb, ub in zip(lower_bound_floated, upper_bound_floated):
            X.append(np.linspace(lb, ub, 500))

        Y = []
        for func, x in zip(sp_f, X):
            f = sp.lambdify(x_s, func, "numpy")
            Y.append(f(x))

        fig, ax = plt.subplots()
        ax.grid(True)

        for f_name, x, y, lb, ub in zip(
            functions, X, Y, lower_bound_floated, upper_bound_floated
        ):
            f_name = f_name.replace(" ", "")
            ax.plot(x, y, label=rf"${f_name}$ : [{lb}, {ub}]")

        ax2 = ax.secondary_xaxis("top", functions=(self.rad_to_deg, self.deg_to_rad))
        ax.legend()
        ax.set_xlabel("X (radians)")
        ax2.set_xlabel("X (degrees)")
        ax.set_ylabel("f(X)")
        ax2.set_xlim(ax.get_xlim())
        if f"{name_param}" in os.listdir("./users_graphics"):
            plt.savefig(f"./users_graphics/{name_param}/SIMPLE.png")
        else:
            os.mkdir(f"./users_graphics/{name_param}")
            plt.savefig(f"./users_graphics/{name_param}/SIMPLE.png")
        plt.close("all")

        return f"./users_graphics/{name_param}/SIMPLE.png"
