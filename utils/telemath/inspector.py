import sympy as sp
import numpy as np


class Inspector:
    def __init__(self) -> None:
        pass

    def is_calc(self, expr: str) -> bool:
        try:
            sp.sympify(expr).evalf()
        except:
            return False
        return True

    def is_func_val_in_point(self, expr: str) -> bool:
        splitted = expr.split(";")
        if len(splitted) != 2:
            return False
        try:
            function = sp.sympify(splitted[0])
        except:
            return False
        values = splitted[1].split()
        x = sp.Symbol("x")
        for val in values:
            try:
                float(val)
            except:
                return False
        for val in values:
            try:
                function.subs(x, float(val))
            except:
                return False
        return True

    def is_to_scientific(self, values: str):
        values = values.split()
        for val in values:
            try:
                float(val)
            except:
                return False
        return True

    def is_undefind_integral(self, expr: str):
        expr = expr.split("\n")
        x = sp.Symbol("x")
        for integral in expr:
            try:
                sp.integrate(integral, x)
            except:
                return False
        return True

    def is_integral(self, integrals: str) -> str:
        integrals = integrals.split("\n")
        integrals_ = []
        x = sp.Symbol("x")
        bounds = []
        for integral in integrals:
            integrals_.append(integral.split(";")[0])
            try:
                bounds.append(integral.split(";")[1])
            except:
                return False
        lower_bounds = []
        upper_bounds = []
        for bound in bounds:
            lower_bounds.append(bound.split()[0])
            try:
                upper_bounds.append(bound.split()[1])
            except:
                return False
        for lb, ub in zip(lower_bounds, upper_bounds):
            try:
                float(lb)
            except:
                return False
            try:
                float(ub)
            except:
                return False
            if float(lb) > float(ub):
                return False
            elif float(lb) == float(ub):
                return False
        for i, lb, ub in zip(integrals_, lower_bounds, upper_bounds):
            try:
                sp.integrate(i, (x, float(lb), float(ub)))

            except:
                return False
        return True

    def is_std(self, values: str) -> str:
        values = values.split()
        if len(values) == 1:
            return False
        for val in values:
            try:
                float(val)
            except:
                return False
        return True

    def is_mean(self, values: str) -> str:
        values = values.split()
        for val in values:
            try:
                float(val)
            except:
                return False
        return True

    def is_derr(self, exprs: str) -> str:
        exprs = exprs.split("\n")
        x = sp.Symbol("x")
        for expr in exprs:
            try:
                sp.diff(expr, x)
            except:
                return False
        return True

    def is_interp(self, data: str) -> bool:
        data = data.split("\n")
        X_str = []
        Y_str = []
        for elem in data:
            elem = elem.split(";")
            if len(elem) == 2:
                X_str.append(elem[0])
                Y_str.append(elem[1])
            else:
                return False
        X = []
        Y = []
        for x, y in zip(X_str, Y_str):
            if len(x.split()) != len(y.split()):
                return False
            x_ = []
            y_ = []
            for i, j in zip(x.split(), y.split()):
                try:
                    x_.append(float(i))
                except:
                    return False
                try:
                    y_.append(float(j))
                except:
                    return False
            X.append(x_)
            Y.append(y_)
        return True

    def is_approx(self, data: str) -> bool:
        data = data.split("\n")
        X_str = []
        Y_str = []
        degrees_str = []
        for elem in data:
            elem = elem.split(";")
            if len(elem) == 2:
                X_str.append(elem[0])
                Y_str.append(elem[1])
                degrees_str.append(None)
            elif len(elem) == 3:
                X_str.append(elem[0])
                Y_str.append(elem[1])
                degrees_str.append(elem[2])
            else:
                return False
        X = []
        Y = []
        degree = []
        for x, y, d in zip(X_str, Y_str, degrees_str):
            if len(x.split()) != len(y.split()):
                return False
            x_ = []
            y_ = []
            for i, j in zip(x.split(), y.split()):
                try:
                    x_.append(float(i))
                except:
                    return False
                try:
                    y_.append(float(j))
                except:
                    return False
            X.append(x_)
            Y.append(y_)
            if d is not None:
                try:
                    degree.append(float(d))
                except:
                    return False
            else:
                degree.append(None)
        return True

    def is_simple(self, data: str) -> bool:
        exprs = data.split("\n")
        functions = []
        bounds = []
        x = sp.Symbol("x")
        for expr in exprs:
            expr = expr.split(";")
            functions.append(expr[0])
            if len(expr) == 2:
                bounds.append(expr[1])
            else:
                bounds.append(None)
        for func in functions:
            try:
                sp.sympify(func)
            except:
                return False
        lower_bound = []
        upper_bound = []
        for bound in bounds:
            if bound is not None:
                bound = bound.split()
                try:
                    lower_bound.append(bound[0])
                except:
                    return False
                if len(bound) == 2:
                    upper_bound.append(bound[1])
                elif len(bound) == 1:
                    upper_bound.append(None)
                else:
                    return False
            else:
                lower_bound.append(None)
                upper_bound.append(None)
        for lb, ub in zip(lower_bound, upper_bound):
            if lb is not None and ub is not None:
                if float(lb) > float(ub):
                    return False
                elif float(lb) == float(ub):
                    return False
        for lb, ub in zip(lower_bound, upper_bound):
            if lb is not None:
                try:
                    float(lb)
                except:
                    return False
            if ub is not None:
                try:
                    float(ub)
                except:
                    return False
        test_x = np.array([1])
        for func in functions:
            try:
                f = sp.lambdify(x, func, "numpy")
            except:
                return False
            try:
                f(test_x)
            except:
                return False
        return True
