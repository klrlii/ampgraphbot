import numpy as np
import sympy as sp


class Tracer:
    def __init__(self) -> None:
        pass

    def calc(self, expr: str) -> str:
        symbols = ["+", "-", "*", "/"]
        for symbol in symbols:
            expr = expr.replace(symbol, "|")
        expr = expr.split("|")

        if len(expr) == 1:
            return f"{expr[0]} - невозможно привести к типу математического выражения"

        splitted = []
        for element in expr:
            if element != "":
                splitted.append(element)

        problematic = "[\n"
        for element in splitted:
            try:
                sp.sympify(element)
            except:
                problematic += f"{element}\n"
        problematic += "]\nневозможно привести к типу математического выражения"
        return problematic

    def func_val_in_point(self, expr) -> str:
        problematic = ""
        splitted = expr.split(";")
        if len(splitted) != 2:
            return problematic + f"[{expr}]\nотсутствует точка с запятой"
        try:
            function = sp.sympify(splitted[0])
        except:
            return problematic + f"[{splitted[0]}]\nне является функцией"
        values = splitted[1].split()
        x = sp.Symbol("x")
        for val in values:
            try:
                float(val)
            except:
                problematic += f"[{val}] - не является числом\n"
        for val in values:
            try:
                function.subs(x, float(val))
            except:
                problematic += f"[{val}] - невозможно подставить число в выражение\n"
        return problematic

    def to_scientific(self, values: str) -> str:
        values = values.split()
        problematic = ""
        for val in values:
            try:
                float(val)
            except:
                problematic += f"[{val}] - не является числом\n"
        return problematic

    def undefind_integral(self, integrals: str) -> str:
        problematic = ""
        integrals = integrals.split("\n")
        x = sp.Symbol("x")
        for integral in integrals:
            try:
                sp.integrate(integral, x)
            except:
                problematic += f"[{integral}] - невозможно получить неопределенный интеграл от данного выражения\n"
        return problematic

    def integral(self, integrals: str) -> str:
        problematic = ""
        x = sp.Symbol("x")
        integrals = integrals.split("\n")
        integrals_ = []
        bounds = []
        for integral in integrals:
            integrals_.append(integral.split(";")[0])
            try:
                bounds.append(integral.split(";")[1])
            except:
                problematic += f"[{integral}] - неверный синтаксис\n"
                return problematic.rstrip()
        lower_bounds = []
        upper_bounds = []
        for bound in bounds:
            lower_bounds.append(bound.split()[0])
            try:
                upper_bounds.append(bound.split()[1])
            except:
                problematic += f"[{bound}] - отсутствует верхняя граница\n"
                return problematic.rstrip()
        for lb, ub in zip(lower_bounds, upper_bounds):
            try:
                float(lb)
            except:
                problematic += f"[{lb}] - не является числом\n"
                return problematic
            try:
                float(ub)
            except:
                problematic += f"[{ub}] - не является числом\n"
                return problematic
            if float(lb) > float(ub):
                problematic += f"[{lb}] > [{ub}] - нижняя граница больше верхней\n"
                return problematic
            elif float(lb) == float(ub):
                problematic += f"[{lb}] = [{ub}] - нижняя граница равна верхней\n"
                return problematic
        for i, lb, ub in zip(integrals_, lower_bounds, upper_bounds):
            try:
                sp.integrate(i, (x, float(lb), float(ub)))
            except:
                problematic += f"[{i}], [{lb}], [{ub}] - невозможно проинтегрировать данное выражение\n"
        return problematic

    def std(self, values: str) -> str:
        problematic = ""
        values = values.split()
        if len(values) == 1:
            return (
                problematic + f"{values} - невозможно определить СКО от одного значения"
            )
        for val in values:
            try:
                float(val)
            except:
                problematic += f"[{val}] - не является числом\n"
        return problematic.rstrip()

    def mean(self, values: str) -> str:
        problematic = ""
        values = values.split()
        for val in values:
            try:
                float(val)
            except:
                problematic += f"[{val}] - не является числом\n"
        return problematic.rstrip()

    def derr(self, exprs: str) -> str:
        problematic = ""
        exprs = exprs.split("\n")
        x = sp.Symbol("x")
        for expr in exprs:
            try:
                sp.diff(expr, x)
            except:
                problematic += f"[{expr}] - невозможно продифференцировать\n"
        return problematic.rstrip()

    def interp(self, data: str) -> str:
        problematic = ""
        data = data.split("\n")
        X_str = []
        Y_str = []
        for elem in data:
            elem = elem.split(";")
            if len(elem) == 2:
                X_str.append(elem[0])
                Y_str.append(elem[1])
            else:
                return f"{elem} - ошибка синтаксиса"
        X = []
        Y = []
        for x, y in zip(X_str, Y_str):
            if len(x.split()) != len(y.split()):
                return f"{x.split()}; {y.split()} - несоответствие размерностей : ({len(x.split())}, {len(y.split())})"
            x_ = []
            y_ = []
            for i, j in zip(x.split(), y.split()):
                try:
                    x_.append(float(i))
                except:
                    problematic += (
                        f"{i} - не является числом\n(\n{x.split()}\n{y.split()}\n)\n"
                    )
                try:
                    y_.append(float(j))
                except:
                    problematic += (
                        f"{j} - не является числом\n(\n{x.split()}\n{y.split()}\n)\n"
                    )
            X.append(x_)
            Y.append(y_)
        return problematic.rstrip()

    def approx(self, data: str) -> str:
        problematic = ""
        data = data.split("\n")
        X_str = []
        Y_str = []
        degrees_str = []
        for elem in data:
            elem = elem.split(";")
            if len(elem) == 2:
                X_str.append(elem[0])
                Y_str.append(elem[1])
                degrees_str.append(None)
            elif len(elem) == 3:
                X_str.append(elem[0])
                Y_str.append(elem[1])
                degrees_str.append(elem[2])
            else:
                return f"{elem} - ошибка синтаксиса"
        X = []
        Y = []
        degree = []
        for x, y, d in zip(X_str, Y_str, degrees_str):
            if len(x.split()) != len(y.split()):
                return f"{x.split()}; {y.split()} - несоответствие размерностей : ({len(x.split())}, {len(y.split())})"
            x_ = []
            y_ = []
            for i, j in zip(x.split(), y.split()):
                try:
                    x_.append(float(i))
                except:
                    problematic += (
                        f"{i} - не является числом\n(\n{x.split()}\n{y.split()}\n)\n"
                    )
                try:
                    y_.append(float(j))
                except:
                    problematic += (
                        f"{j} - не является числом\n(\n{x.split()}\n{y.split()}\n)\n"
                    )
            X.append(x_)
            Y.append(y_)
            if d is not None:
                try:
                    degree.append(float(d))
                except:
                    problematic += f"{d} - не является числом\n(\n{x}\n{y}\n{d}\n)\n"
            else:
                degree.append(None)
        return problematic.rstrip()

    def simple(self, data: str) -> str:
        problematic = ""
        exprs = data.split("\n")
        functions = []
        bounds = []
        x = sp.Symbol("x")
        for expr in exprs:
            expr = expr.split(";")
            functions.append(expr[0])
            if len(expr) == 2:
                bounds.append(expr[1])
            else:
                bounds.append(None)
        for func in functions:
            try:
                sp.sympify(func)
            except:
                problematic += f"[{func}] - не является функцией\n"
        if problematic != "":
            return problematic.rstrip()
        lower_bound = []
        upper_bound = []
        for bound, i in zip(bounds, range(len(functions))):
            if bound is not None:
                bound = bound.split()
                try:
                    lower_bound.append(bound[0])
                except:
                    pass
                if len(bound) == 2:
                    upper_bound.append(bound[1])
                elif len(bound) == 1:
                    upper_bound.append(None)
                else:
                    problematic += f"{bound} - ошибка синатксиа : отсутствие границ или количество границ > 2 ({functions[i]} : {bound})\n"
            else:
                lower_bound.append(None)
                upper_bound.append(None)
        if problematic != "":
            return problematic.rstrip()
        for lb, ub, i in zip(lower_bound, upper_bound, range(len(functions))):
            if lb is not None and ub is not None:
                if float(lb) > float(ub):
                    problematic += f"[{lb}] > [{ub}] - нижняя граница больше верхней [{functions[i]} : {lb} {ub}]\n"
                elif float(lb) == float(ub):
                    problematic += f"[{lb}] = [{ub}] - нижняя граница равна верхней [{functions[i]} : {lb} {ub}]\n"
        if problematic != "":
            return problematic.rstrip()
        for lb, ub, i in zip(lower_bound, upper_bound, range(len(functions))):
            if lb is not None:
                try:
                    float(lb)
                except:
                    problematic += f"[{lb}] - граница не является числом [{functions[i]} : {lb}, {ub}]\n"
            if ub is not None:
                try:
                    float(ub)
                except:
                    problematic += f"[{ub}] - граница не является числом [{functions[i]} : {lb}, {ub}]\n"
        if problematic != "":
            return problematic.rstrip()
        test_x = np.array([1])
        for func in functions:
            try:
                f = sp.lambdify(x, func, "numpy")
            except:
                problematic += f"[{func}] - ошибка в определении функции\n"
            try:
                f(test_x)
            except:
                problematic += f"[{func}] - ошибка в определении функции\n"
        return problematic.rstrip()
