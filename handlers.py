from aiogram import types, F, Router
from aiogram.types import Message
from aiogram.filters import Command

from aiogram import flags
from aiogram.fsm.context import FSMContext
from aiogram.types.callback_query import CallbackQuery

import kb
from utils import Inspector, Processor, Tracer
from states import General, Algebra, Graphs, menu_state
from text import texts


router = Router()
inspect = Inspector()
process = Processor()
trace = Tracer()


# commands


@router.message(Command("start"))
async def start_handler(msg: Message):
    await msg.answer(
        texts["start"].format(name=msg.from_user.full_name), reply_markup=kb.menu
    )


@router.message(Command("menu"))
async def menu_button_handler(msg: Message, state: FSMContext):
    await state.set_state(menu_state)
    await msg.answer(texts["menu"], reply_markup=kb.menu)


# ----------------------------------
#             call backs
# ----------------------------------


@router.callback_query(F.data == "to_main")
async def menu(clback: CallbackQuery, state: FSMContext):
    await state.set_state(menu_state)
    await clback.message.delete()
    await clback.message.answer(texts["menu"], reply_markup=kb.menu)


@router.callback_query(F.data == "algebra")
async def algebra(clback: CallbackQuery, state: FSMContext):
    await state.set_state(General.algebra_state)
    await clback.message.delete()
    await clback.message.answer(texts["algebra"], reply_markup=kb.algebra)


@router.callback_query(F.data == "graphs")
async def graphs(clback: CallbackQuery, state: FSMContext):
    await state.set_state(General.graphs_state)
    await clback.message.delete()
    await clback.message.answer(texts["graphs"], reply_markup=kb.graphs)


@router.callback_query(F.data == "donate")
async def support(clback: CallbackQuery, state: FSMContext):
    await state.set_state(General.donate_state)
    await clback.message.delete()
    await clback.message.answer(texts["donate"], reply_markup=kb.iexit_kb)


# ---------Algebra---------


@router.callback_query(F.data == "calc")
async def calc(clback: CallbackQuery, state: FSMContext):
    await state.set_state(Algebra.calc_state)
    await clback.message.delete()
    await clback.message.answer(texts["calc"], reply_markup=kb.go_back_to_algebra)


@router.callback_query(F.data == "func_val_in_point")
async def func_val_in_point(clback: CallbackQuery, state: FSMContext):
    await state.set_state(Algebra.func_val_in_point_state)
    await clback.message.delete()
    await clback.message.answer(
        texts["func_val_in_point"], reply_markup=kb.go_back_to_algebra
    )


@router.callback_query(F.data == "to_scientific")
async def to_scientific(clback: CallbackQuery, state: FSMContext):
    await state.set_state(Algebra.to_scientific_state)
    await clback.message.delete()
    await clback.message.answer(
        texts["to_scientific"], reply_markup=kb.go_back_to_algebra
    )


@router.callback_query(F.data == "undefind_integral")
async def undefind_integral(clback: CallbackQuery, state: FSMContext):
    await state.set_state(Algebra.undefind_integral_state)
    await clback.message.edit_text(
        texts["undefind_integral"], reply_markup=kb.go_back_to_algebra
    )


@router.callback_query(F.data == "integral")
async def integral(clback: CallbackQuery, state: FSMContext):
    await state.set_state(Algebra.integral_state)
    await clback.message.delete()
    await clback.message.answer(texts["integral"], reply_markup=kb.go_back_to_algebra)


@router.callback_query(F.data == "std")
async def std(clback: CallbackQuery, state: FSMContext):
    await state.set_state(Algebra.std_state)
    await clback.message.delete()
    await clback.message.answer(texts["std"], reply_markup=kb.go_back_to_algebra)


@router.callback_query(F.data == "mean")
async def mean(clback: CallbackQuery, state: FSMContext):
    await state.set_state(Algebra.mean_state)
    await clback.message.delete()
    await clback.message.answer(texts["mean"], reply_markup=kb.go_back_to_algebra)


@router.callback_query(F.data == "derr")
async def derr(clback: CallbackQuery, state: FSMContext):
    await state.set_state(Algebra.derr_state)
    await clback.message.delete()
    await clback.message.answer(texts["derr"], reply_markup=kb.go_back_to_algebra)


# ---------Graphs---------


@router.callback_query(F.data == "interp")
async def interp(clback: CallbackQuery, state: FSMContext):
    await state.set_state(Graphs.interp_state)
    await clback.message.delete()
    await clback.message.answer(texts["interp"], reply_markup=kb.go_back_to_graphs)


@router.callback_query(F.data == "approx")
async def approx(clback: CallbackQuery, state: FSMContext):
    await state.set_state(Graphs.approx_state)
    await clback.message.delete()
    await clback.message.answer(texts["approx"], reply_markup=kb.go_back_to_graphs)


@router.callback_query(F.data == "simple")
async def simple(clback: CallbackQuery, state: FSMContext):
    await state.set_state(Graphs.simple_state)
    await clback.message.delete()
    await clback.message.answer(texts["simple"], reply_markup=kb.go_back_to_graphs)


@router.callback_query(F.data == "optimise")
async def optimise(clback: CallbackQuery, state: FSMContext):
    await clback.message.edit_text(texts["wait_on_inspector"])
    if inspect.is_to_scientific(clback.message.text) == False:
        await clback.message.edit_text(texts["wait_on_error_found"])
        err = trace.to_scientific(clback.message.text)
        return await clback.message.edit_text(texts["try_repeat"].format(err=err))
    await clback.message.edit_text(texts["wait_on_process"])
    await clback.message.edit_text(process.to_scientific(clback.message.text))


# --------------------------------
#             messages
# --------------------------------


# menu


@router.message(menu_state)
@flags.chat_action("typing")
async def default_menu_answer(msg: Message):
    await msg.answer(texts["default_menu_answer"], reply_markup=kb.menu)


@router.message(General.algebra_state)
@flags.chat_action("typing")
async def default_algebra_answer(msg: Message):
    await msg.answer(texts["default_algebra_answer"], reply_markup=kb.algebra)


@router.message(General.graphs_state)
@flags.chat_action("typing")
async def default_graphs_answer(msg: Message):
    await msg.answer(texts["default_graphs_answer"], reply_markup=kb.graphs)


# algebra


@router.message(Algebra.calc_state)
@flags.chat_action("typing")
async def calc_process(msg: Message):
    mesg = await msg.answer(texts["wait_on_inspector"])
    if inspect.is_calc(msg.text) == False:
        await mesg.edit_text(texts["wait_on_error_found"])
        err = trace.calc(msg.text)
        return await mesg.edit_text(texts["try_repeat"].format(err=err))
    await mesg.edit_text(texts["wait_on_process"])
    await mesg.edit_text(process.calc(msg.text), reply_markup=kb.need_to_optimise)


@router.message(Algebra.func_val_in_point_state)
@flags.chat_action("typing")
async def func_val_in_point_process(msg: Message):
    mesg = await msg.answer(texts["wait_on_inspector"])
    if inspect.is_func_val_in_point(msg.text) == False:
        await mesg.edit_text(texts["wait_on_error_found"])
        err = trace.func_val_in_point(msg.text)
        return await mesg.edit_text(texts["try_repeat"].format(err=err))
    await mesg.edit_text(texts["wait_on_process"])
    await mesg.edit_text(
        process.func_val_in_point(msg.text), reply_markup=kb.need_to_optimise
    )


@router.message(Algebra.to_scientific_state)
@flags.chat_action("typing")
async def to_scientific_process(msg: Message):
    mesg = await msg.answer(texts["wait_on_inspector"])
    if inspect.is_to_scientific(msg.text) == False:
        await mesg.edit_text(texts["wait_on_error_found"])
        err = trace.to_scientific(msg.text)
        return await mesg.edit_text(texts["try_repeat"].format(err=err))
    await mesg.edit_text(texts["wait_on_process"])
    await mesg.edit_text(process.to_scientific(msg.text))


@router.message(Algebra.undefind_integral_state)
@flags.chat_action("typing")
async def undefind_integral_process(msg: Message):
    mesg = await msg.answer(texts["wait_on_inspector"])
    if inspect.is_undefind_integral(msg.text) == False:
        await mesg.edit_text(texts["wait_on_error_found"])
        err = trace.undefind_integral(msg.text)
        return await mesg.edit_text(texts["try_repeat"].format(err=err))
    await mesg.edit_text(texts["wait_on_process"])
    await mesg.edit_text(process.undefind_integral(msg.text))


@router.message(Algebra.integral_state)
@flags.chat_action("typing")
async def integral_process(msg: Message):
    mesg = await msg.answer(texts["wait_on_inspector"])
    if inspect.is_integral(msg.text) == False:
        await mesg.edit_text(texts["wait_on_error_found"])
        err = trace.integral(msg.text)
        return await mesg.edit_text(texts["try_repeat"].format(err=err))
    await mesg.edit_text(texts["wait_on_process"])
    await mesg.edit_text(process.integral(msg.text), reply_markup=kb.need_to_optimise)


@router.message(Algebra.std_state)
@flags.chat_action("typing")
async def std_process(msg: Message):
    mesg = await msg.answer(texts["wait_on_inspector"])
    if inspect.is_std(msg.text) == False:
        await mesg.edit_text(texts["wait_on_error_found"])
        err = trace.std(msg.text)
        return await mesg.edit_text(texts["try_repeat"].format(err=err))
    await mesg.edit_text(texts["wait_on_process"])
    await mesg.edit_text(process.std(msg.text), reply_markup=kb.need_to_optimise)


@router.message(Algebra.mean_state)
@flags.chat_action("typing")
async def mean_process(msg: Message):
    mesg = await msg.answer(texts["wait_on_inspector"])
    if inspect.is_mean(msg.text) == False:
        await mesg.edit_text(texts["wait_on_error_found"])
        err = trace.mean(msg.text)
        return await mesg.edit_text(texts["try_repeat"].format(err=err))
    await mesg.edit_text(texts["wait_on_process"])
    await mesg.edit_text(process.mean(msg.text))


@router.message(Algebra.derr_state)
@flags.chat_action("typing")
async def derr_process(msg: Message):
    mesg = await msg.answer(texts["wait_on_inspector"])
    if inspect.is_derr(msg.text) == False:
        await mesg.edit_text(texts["wait_on_error_found"])
        err = trace.derr(msg.text)
        return await mesg.edit_text(texts["try_repeat"].format(err=err))
    await mesg.edit_text(texts["wait_on_process"])
    await mesg.edit_text(process.derr(msg.text))


# graphs


@router.message(Graphs.interp_state)
@flags.chat_action("typing")
async def interp_process(msg: Message):
    mesg = await msg.answer(texts["wait_on_inspector"])
    if inspect.is_interp(msg.text) == False:
        await mesg.edit_text(texts["wait_on_error_found"])
        err = trace.interp(msg.text)
        return await mesg.edit_text(texts["try_repeat"].format(err=err))
    await mesg.edit_text(texts["wait_on_process"])
    await mesg.answer_photo(
        types.FSInputFile(process.interp(msg.text, msg.from_user.id))
    )
    await mesg.delete()


@router.message(Graphs.approx_state)
@flags.chat_action("typing")
async def approx_process(msg: Message):
    mesg = await msg.answer(texts["wait_on_inspector"])
    if inspect.is_approx(msg.text) == False:
        await mesg.edit_text(texts["wait_on_error_found"])
        err = trace.approx(msg.text)
        return await mesg.edit_text(texts["try_repeat"].format(err=err))
    await mesg.edit_text(texts["wait_on_process"])
    await mesg.answer_photo(
        types.FSInputFile(process.approx(msg.text, msg.from_user.id))
    )
    await mesg.answer(process.poly_expr(msg.text))
    await mesg.delete()


@router.message(Graphs.simple_state)
@flags.chat_action("typing")
async def simple_process(msg: Message):
    mesg = await msg.answer(texts["wait_on_inspector"])
    if inspect.is_simple(msg.text) == False:
        await mesg.edit_text(texts["wait_on_error_found"])
        err = trace.simple(msg.text)
        return await mesg.edit_text(texts["try_repeat"].format(err=err))
    await mesg.edit_text(texts["wait_on_process"])
    await mesg.answer_photo(
        types.FSInputFile(process.simple(msg.text, msg.from_user.id))
    )
    await mesg.delete()
